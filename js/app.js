/*Теоретичні питання

1. Як можна сторити функцію та як ми можемо її викликати?
функція створюється за допомогою ключового слова function, а викликається за допомогою назви функції та (), наприклад, name();

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
Оператор return в JavaScript завершує виконання функції та повертає вказане значення.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Параметри — це змінні, що вказуються в оголошенні функції та отримують значення при виклику функції.
Аргументи — це конкретні значення, які передаються функції під час її виклику.

4. Як передати функцію аргументом в іншу функцію?
Функцію можна передати як аргумент іншій функції, вказавши їх в дужках при виклику, наприклад, name(1,2);

Практичні завдання

1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.*/

function part() {
    let a, b;
    do {
        a = +prompt("Enter first number");
        b = +prompt("Enter second number");
    } while (isNaN(a) || isNaN(b));

    console.log(a-b);
}
part();

//2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

function calc() {
    let firstNumber, secondNumber;
    do {
        firstNumber = +prompt("Enter first number");
        secondNumber = +prompt("Enter second number");
    } while (isNaN(firstNumber) || isNaN(secondNumber));
    let operator;
    do {
        operator = prompt("Enter operator");
        if (operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/') {
            alert('Такої операції не існує');
        }
    } while (operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/');

    let result;
    switch (operator) {
        case '+':
            result = firstNumber + secondNumber;
            break;
        case '-':
            result = firstNumber - secondNumber;
            break;
        case '*':
            result = firstNumber * secondNumber;
            break;
        case '/':
            result = firstNumber / secondNumber;
            break;
    }
    console.log(result);
}

calc();